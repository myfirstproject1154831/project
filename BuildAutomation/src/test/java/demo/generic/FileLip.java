package demo.generic;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FileLip {

	public String getData(String sheetName,int rowNum,int cellNum)throws EncryptedDocumentException, IOException {
		
		FileInputStream fis=new FileInputStream("./src/test/resources/practiceExcel.xlsx");
		Workbook work=WorkbookFactory.create(fis);
		String data = work.getSheet(sheetName).getRow(rowNum).getCell(cellNum).getStringCellValue();
		
		return data;
		
	}
	public void setData(String sheetName,int rowNum,int cellNum,String update) throws EncryptedDocumentException, IOException {
		
		FileInputStream fis=new FileInputStream("./src/test/resources/practiceExcel.xlsx");
		Workbook work = WorkbookFactory.create(fis);
		          work.getSheet(sheetName).getRow(rowNum).getCell(cellNum).setCellValue(update);
		          FileOutputStream fiout=new FileOutputStream("./src/test/resources/practiceExcel.xlsx");
		          work.write(fiout);
		          
	}
}
