package demo.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class POM_test {

	@FindBy(id="m_name")
	private WebElement custName;
	@FindBy(xpath="//label[text()='Category:']")
	private WebElement categName;
	@FindBy(id="m_subscribe")
	private WebElement checkBox;
	@FindBy(xpath="//button[text()='Save']")
	private WebElement savButton;
	@FindBy(xpath="//button[text()='Cancel']")
	private WebElement cancel;
	public POM_test(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public WebElement getCustName() {
		return custName;
	}
	public void setCustName(WebElement custName) {
		this.custName = custName;
	}
	public WebElement getCategName() {
		return categName;
	}
	public void setCategName(WebElement categName) {
		this.categName = categName;
	}
	public WebElement getCheckBox() {
		return checkBox;
	}
	public void setCheckBox(WebElement checkBox) {
		this.checkBox = checkBox;
	}
	public WebElement getSavButton() {
		return savButton;
	}
	public void setSavButton(WebElement savButton) {
		this.savButton = savButton;
	}
	public WebElement getCancel() {
		return cancel;
	}
	public void setCancel(WebElement cancel) {
		this.cancel = cancel;
	}
	
}
